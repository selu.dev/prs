use std::num::ParseIntError;

use clap::{builder::ValueParser, Arg, ArgMatches};

use super::{CmdArg, CmdArgOption};

fn validate_number(value: &str) -> Result<String, ParseIntError> {
    // Validate the number but do not convert
    let _val: usize = value.parse()?;

    Ok(value.to_string())
}

pub struct ArgPrevious {}

impl CmdArg for ArgPrevious {
    fn name() -> &'static str {
        "previous"
    }

    fn build() -> Arg {
        Arg::new("previous")
            .long("previous")
            .short('P')
            .value_name("PAST")
            .num_args(0..=1)
            .require_equals(true)
            .default_value("0")
            .default_missing_value("1")
            .hide_default_value(true)
            .value_parser(ValueParser::new(validate_number))
            .help("Get previous values by index")
    }
}

impl<'a> CmdArgOption<'a> for ArgPrevious {
    type Value = Option<usize>;

    fn value(matches: &'a ArgMatches) -> Self::Value {
        Self::value_raw(matches)
            .and_then(|v| v.parse().ok())
            .and_then(|p| if p == 0 { None } else { Some(p) })
    }
}
